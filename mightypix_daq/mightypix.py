import basil.dut
import sys
import time
import os
import numpy as np
import logging

# import warnings
import yaml
import collections

sys.path = [os.path.dirname(os.path.abspath(__file__))] + sys.path
OUTPUT_DIR = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "output_data")


def format_power(dat):  # TODO improve more...
    s = "power:"
    for pwr in ['VDDA', 'VDDD', 'VDD_BCID_BUFF', 'VPC', "PBias", "NTC",
                'BL', 'TH', 'VCascC', 'VCascN']:
        s = s+" %s=%.4fV(%fmA)" % (pwr, dat[pwr+'[V]'], dat[pwr+'[mA]'])
    return s


def format_dac(dat):  # TODO improve more...
    s = "DAC:"
    for dac in ['BLRes', 'VAmp', 'VPFB', 'VPFoll', 'VPLoad', 'IComp', 'Vbias_CS', 'IBOTA', 'ILVDS', 'Vfs', 'LSBdacL', 'Vsf_dis1', 'Vsf_dis2', 'Vsf_dis3']:
        s = s+" %s=%d" % (dac, dat[dac])
    return s


def format_pix(dat):  # TODO improve more...
    s = "Pixels:"
    return s


def mk_fname(ext="data.npy", dirname=None):
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(OUTPUT_DIR, prefix)
    if not os.path.exists(dirname):
        os.system("mkdir -p %s" % dirname)
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_")+ext)


class MightyPix():
    default_yaml = os.path.dirname(os.path.abspath(__file__)) + os.sep + "mightypix.yaml"

    def __init__(self, conf=None, **kwargs):
        self.logger = logging.getLogger()
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.conf = conf
        # if not conf:
        #     conf = os.path.join(self.proj_dir, 'tpx3' + os.sep + 'tpx3.yaml')
        #
        # self.logger.info("Loading configuration file from %s" % conf)
        # super(MightyPix, self).__init__(conf)


    def init(self, dut=None, no_power_reset=True):
        # set logger

        logFormatter = logging.Formatter(
            "%(asctime)s [%(levelname)-5.5s] (%(threadName)-10s) %(message)s")
        fname = mk_fname(ext="log.log")
        fileHandler = logging.FileHandler(fname)
        fileHandler.setFormatter(logFormatter)
        self.logger.addHandler(fileHandler)

        self.logger.info("MightyPix initialized at " +
                         time.strftime("%Y-%m-%d_%H:%M:%S"))

        self.debug = 0
        self.inj_device = "gpac"
        # self.COL_SIZE = 36  ##TODO this will be used in scans... maybe not here
        # self.ROW_SIZE = 129

        if dut is None:
            dut = self.default_yaml

        if isinstance(dut, str):
            self.dut = basil.dut.Dut(conf=self.conf)

        elif isinstance(dut, MightyPix):
            self.dut = dut

        self.COL_SIZE = self.dut._conf["ncols"]
        self.ROW_SIZE = self.dut._conf["nrows"]
        # self.COL_SIZE = 124
        # self.ROW_SIZE = 29

        # for each of the collumns there is a set of 5 bits that
        self.dut.PIXEL_CONF = collections.OrderedDict()
        # //bit 0,1,2,3:              hit buffer cel 0-3
        # //bit 4:                    pixel ram for all 3 cells
        # //bit 5-16:                 5 bit hit buffer RAm write and 7 bit Wr RAM
        # //bit 5,6,7,8,9:            WR hit buffer row (col even: ascending order; col odd: descending order)!!!
        # //bit 10,11,12,13,14,15,16: pixel WR ram (col even: ascending order; col odd: descending order)!!!
        # //bit 17,18,19,20,21:       enInj row (col even: ascending order; col odd: descending order)!!!
        # //bit 22:                   HB_B column
        # //bit 23:                   Amp column
        # //bit 24:                   enInj col
        self.dut.PIXEL_CONF['HIT_BUFFER_RAM'] = np.full([self.COL_SIZE, 4], True, dtype=np.bool)
        self.dut.PIXEL_CONF['HIT_BUFFER_RAM_ALL'] = np.full([self.COL_SIZE, 1], True, dtype=np.bool)
        self.dut.PIXEL_CONF['HIT_BUFFER_RAM_WRITE_ROW'] = [np.full([self.COL_SIZE, 5], True, dtype=np.uint8), 5]
        self.dut.PIXEL_CONF['HIT_BUFFER_RAM_WRITE_PIX_WR'] = [np.full([self.COL_SIZE, 7], True, dtype=np.uint8), 7]
        # Notice there are 3 RAM write signals/pixel row. There can be up to 124 pixels in one column,
        # however only 62 with in pixel RAM. Since there are only 62 rows with pixel RAM, there are
        # 62x3=186 RAM write signals in total. The lowest 62 pixel never have RAM, the first row that
        # can have RAM is row62.
        # Example= RAM(0=6) means RAM(0=2)_row62, RAM(0=2)_row63, RAM(0)_row64
        self.dut.PIXEL_CONF["INJ_ROW_EN"] = [np.full([self.COL_SIZE, 5], True, dtype=np.uint8), 5]
        self.dut.PIXEL_CONF["HB_COL_EN"] = np.full([self.COL_SIZE, 1], True, dtype=np.bool)
        # Bit 22= enableB (active 0) hit bus for the column – hit bus is the OR function of receiver
        # outputs (after local enable bit) in all hit buffers. Hit bus is designed to be fast – delay 10 ns
        # with respect to hit should be possible.
        self.dut.PIXEL_CONF['AMP_COL_EN'] = np.full([self.COL_SIZE, 1], True, dtype=np.bool)
        self.dut.PIXEL_CONF["INJ_COL_EN"] = np.full([self.COL_SIZE, 1], True, dtype=np.bool)

        self.dut.SET_VALUE = {}
        self.dut.init()
        fw_version = self.dut['intf'].read(0x0, 1)[0]
        logging.info("Firmware version: %s" % (fw_version))

        for reg in self.dut._conf["registers"]:
            if reg["name"] in ["INJ_HI", "INJ_LO"] and "init" in reg:
                self.logger.info("modify %s: %s" % (reg["name"], str(reg["init"])))
                self.dut[reg["name"]]._ch_cal[reg['arg_add']['channel']].update(reg["init"])

        self.dut['CONF']['RESET'] = 1
        self.dut['CONF'].write()
        self.dut['CONF']['RESET'] = 0
        self.dut['CONF'].write()
        self.dut['inj'].reset()
        self.dut['CONF_SR'].set_size(985)

        self.power_up()

        self._write_global_conf()
        # self.set_preamp_en("all")
        # self.set_inj_en([18,25])
        # self.set_mon_en([18,25])
        # self.set_tdac(0)
        # self.dut["gate_tdc"].reset()
        # self.set_inj_all()
        # status=self.power_status()
        # s=format_power(status)
        # self.logger.info(s)

    def _pixel_bitorder(self):
        return_bits = np.empy(0, dtype=np.bool)
        for col in range(self.COL_SIZE):  # loop over all columns
            for register in self.dut.PIXEL_CONF:  # loop over all entries of the pixel config
                print(register)
                # define a bit as the column of each register
                bit = self.dut.PIXEL_CONF[register][col]
                if list == bit.type:  # check the type
                    # creat bit array of each bit and cut the first 3 entries(all 0) away
                    bit = np.unpackbits(bit[0])[8-bit[1]:8]
                    if(col % 2 != 0):  # test for odd colums
                        # for uneven cols the bitorder is reversed!
                        return_bits.append(bit[::-1])  # reverse bit order
                    else:
                        return_bits.append(bit)  # even bit: ascending order
                else:
                    # bool type arrays are directly appended
                    return_bits.append(bit)
        return return_bits

    def _write_global_conf(self):
        self.dut['CONF']['LDDAC'] = 1
        self.dut['CONF'].write()

        self.dut['CONF_SR'].write()
        while not self.dut['CONF_SR'].is_ready:
            time.sleep(0.001)
        self.dut['CONF']['LDDAC'] = 0
        self.dut['CONF'].write()

    # power
    def power_up(self, VSSA=1.2, VGate=2.1, VDDD=1.8, VDDA=1.8,
                 TH1=1.0, TH2=0., TH3=0., BL=1., Vminus=0.0, VCasc2=0.0, NTC=5,
                 INJ_HI=0.6, INJ_LO=0.2):

        # DACS
        self.dut['TH1'].set_voltage(TH1, unit='V')
        self.dut.SET_VALUE['TH1'] = TH1
        self.dut['TH2'].set_voltage(TH2, unit='V')
        self.dut.SET_VALUE['TH2'] = TH2
        self.dut['TH3'].set_voltage(TH3, unit='V')
        self.dut.SET_VALUE['TH3'] = TH3
        self.dut['BL'].set_voltage(BL, unit='V')
        self.dut.SET_VALUE['BL'] = BL

        self.dut['Vminus'].set_current(Vminus, unit='uA')
        self.dut.SET_VALUE['Vminus'] = Vminus
        self.dut['VCasc2'].set_current(VCasc2, unit='uA')
        self.dut.SET_VALUE['VCasc2'] = VCasc2
        self.dut["NTC"].set_current(NTC, unit="uA")
        self.dut.SET_VALUE['NTC'] = NTC

        self.dut['INJ_HI'].set_voltage(INJ_HI, unit='V')
        self.dut.SET_VALUE['INJ_HI'] = INJ_HI
        self.dut['INJ_LO'].set_voltage(INJ_LO, unit='V')
        self.dut.SET_VALUE['INJ_LO'] = INJ_LO

        # POWER
        # the current limit will be for all of the power channels
        # but there is no real limiting of the current so be carefull
        self.dut['VDDD'].set_current_limit(200, unit='mA')
        self.dut['VDDD'].set_voltage(VDDD, unit='V')
        self.dut['VDDD'].set_enable(True)
        self.dut.SET_VALUE['VDDD'] = VDDD

        self.dut['VDDA'].set_voltage(VDDA, unit='V')
        self.dut['VDDA'].set_enable(True)
        self.dut.SET_VALUE['VDDA'] = VDDA

        self.dut['VSSA'].set_voltage(VSSA, unit='V')
        self.dut['VSSA'].set_enable(True)
        self.dut.SET_VALUE['VSSA'] = VSSA

        self.dut['VGate'].set_voltage(VGate, unit='V')
        self.dut['VGate'].set_enable(True)
        self.dut.SET_VALUE['VGate'] = VGate

    def power_down(self):
        for pwr in ['VDDA', 'VDDD', 'VSSA', 'VGate']:
            self.dut[pwr].set_enable(False)

    def power_status(self):
        staus = {}
        for pwr in ['VDDA', 'VDDD', 'VSSA', 'VGate',
                    "TH1", "TH2", "TH3", "BL", "Vminus", "VCasc2", "NTC"]:
            staus[pwr+'[V]'] = self.dut[pwr].get_voltage(unit='V')
            staus[pwr+'[mA]'] = self.dut[pwr].get_current(unit='mA')
            staus[pwr+"set"] = self.dut.SET_VALUE[pwr]
        for dac in ['INJ_LO', 'INJ_HI']:
            staus[dac+"set"] = self.dut.SET_VALUE[dac]
        return staus

    def close(self):
        self.power_down()

if __name__ == "__main__":

    # Create the Pixel object
    chip = MightyPix()
    # chip.init()

    chip.dut["intf"].close()
