
`timescale 1ns / 1ps
`default_nettype none

`include "clk_gen.v"
`include "mightypix_core.v"

`include "utils/bus_to_ip.v"

`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/cdc_pulse_sync.v"

`include "utils/reset_gen.v"
`include "utils/CG_MOD_pos.v"

`include "spi/spi_core.v"
`include "spi/spi.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"

`include "gpio/gpio.v"
`include "gpio/gpio_core.v"

// `include "tlu/tlu_controller.v"
// `include "tlu/tlu_controller_core.v"
// `include "tlu/tlu_controller_fsm.v"

`include "timestamp/timestamp.v"
`include "timestamp/timestamp_core.v"

`include "utils/fx2_to_bus.v"

`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"

`include "sram_fifo/sram_fifo_core.v"
`include "sram_fifo/sram_fifo.v"

`include "utils/3_stage_synchronizer.v"
// `include "rrp_arbiter/rrp_arbiter.v"
// `include "utils/ddr_des.v"
`include "utils/flag_domain_crossing.v"

`include "lib/rec_sync.v"
`include "lib/decode_8b10b.v"
`include "lib/mightypix_data_rx.v"
`include "lib/mightypix_data_rx_core.v"
`include "lib/receiver_logic.v"
`include "utils/cdc_reset_sync.v"

`ifdef COCOTB_SIM //for simulation
    `include "utils/ODDR_sim.v"
    `include "utils/IDDR_sim.v"
    `include "utils/DCM_sim.v"
    `include "utils/clock_multiplier.v"
    `include "utils/BUFG_sim.v"

    `include "utils/RAMB16_S1_S9_sim.v"
`else
    `include "utils/IDDR_s3.v"
    `include "utils/ODDR_s3.v"
`endif


module mightypix_mio (

  input wire FCLK_IN, // 48MHz

    //local bus
    inout wire [7:0] BUS_DATA,
    input wire [15:0] ADD,
    input wire RD_B,
    input wire WR_B,

    //high speed
    inout wire [7:0] FDATA,
    input wire FREAD,
    input wire FSTROBE,
    input wire FMODE,

    //LED
    output wire [4:0] LED,

    //SRAM
    output wire [19:0] SRAM_A,
    inout wire [15:0] SRAM_IO,
    output wire SRAM_BHE_B,
    output wire SRAM_BLE_B,
    output wire SRAM_CE1_B,
    output wire SRAM_OE_B,
    output wire SRAM_WE_B,

    output wire SCK,    //DIN6
    output wire CSB,    //DIN5
    output wire MOSI,   //DIN4
    input wire  MISO,    //DOUT1
    output wire RST_Ctl_RB,   //DIN7
    output wire Ctl_Clk_1,    //DIN3
    output wire Ctl_Clk_2,    //DIN2
    output wire Ctl_Load,     //DIN1 TODO fix conncection
    output wire Ctl_SIN,     //DIN0
    input wire  Ctl_SOut,      //DOUT0

    output wire INJECTION,
    input wire  DATA_LVDS,     //LVDA_OUT0
    output wire Clk_ext_LVDS, //LVDA_IN0
    output wire Clk_ref,      //LVDA_IN1
    output wire Sync_res,     //LVDA_IN2

    // output wire RX_READY
    // input  wire INJECTION_IN,

    // I2C
    inout wire SDA,
    inout wire SCL
);

assign SDA = 1'bz;
assign SCL = 1'bz;


// ------- RESRT/CLOCK  ------- //

wire BUS_RST;

(* KEEP = "{TRUE}" *)
wire CLK320;
(* KEEP = "{TRUE}" *)
wire CLK160;
(* KEEP = "{TRUE}" *)
wire CLK40;
(* KEEP = "{TRUE}" *)
wire CLK16;
(* KEEP = "{TRUE}" *)
wire BUS_CLK;
(* KEEP = "{TRUE}" *)
wire CLK8;

reset_gen reset_gen(.CLK(BUS_CLK), .RST(BUS_RST));

wire CLK_LOCKED;

clk_gen clk_gen(
    .CLKIN(FCLK_IN),
    .BUS_CLK(BUS_CLK),
    .U1_CLK8(CLK8),
    .U2_CLK40(CLK40),
    .U2_CLK16(CLK16),
    .U2_CLK160(CLK160),
    .U2_CLK320(CLK320),
    .U2_LOCKED(CLK_LOCKED)
);

// -------  MODULE ADREESSES  ------- //

localparam FIFO_BASEADDR = 16'h8000;
localparam FIFO_HIGHADDR = 16'h9000-1;

// -------  BUS SYGNALING  ------- //
wire [15:0] BUS_ADD;
wire BUS_RD, BUS_WR;

// -------  BUS SYGNALING  ------- //
fx2_to_bus fx2_to_bus (
    .ADD(ADD),
    .RD_B(RD_B),
    .WR_B(WR_B),

    .BUS_CLK(BUS_CLK),
    .BUS_ADD(BUS_ADD),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .CS_FPGA()
);

// -------  USER MODULES  ------- //

wire FIFO_NEAR_FULL,FIFO_FULL;
wire USB_READ;
wire ARB_READY_OUT, ARB_WRITE_OUT;
wire [31:0] ARB_DATA_OUT;

assign USB_READ = FREAD & FSTROBE;

sram_fifo #(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR)
) sram_fifo (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SRAM_A(SRAM_A),
    .SRAM_IO(SRAM_IO),
    .SRAM_BHE_B(SRAM_BHE_B),
    .SRAM_BLE_B(SRAM_BLE_B),
    .SRAM_CE1_B(SRAM_CE1_B),
    .SRAM_OE_B(SRAM_OE_B),
    .SRAM_WE_B(SRAM_WE_B),

    .USB_READ(USB_READ),
    .USB_DATA(FDATA),

    .FIFO_READ_NEXT_OUT(ARB_READY_OUT),
    .FIFO_EMPTY_IN(!ARB_WRITE_OUT),
    .FIFO_DATA(ARB_DATA_OUT),

    .FIFO_NOT_EMPTY(),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(FIFO_NEAR_FULL),
    .FIFO_READ_ERROR()
);

wire RX_READY;
mightypix_core i_mightypix_core(

      //local bus
      .BUS_CLK(BUS_CLK),
      .BUS_DATA(BUS_DATA),
      .BUS_ADD(BUS_ADD),
      .BUS_RD(BUS_RD),
      .BUS_WR(BUS_WR),
      .BUS_RST(BUS_RST),

      //clocks
      .CLK8(CLK8),
      .CLK40(CLK40),
      .CLK16(CLK16),
      .CLK160(CLK160),
      .CLK320(CLK320),

      //data_out here!!!

      //LED
      .LED(LED[4:0]),

      .SCK(SCK),    //DIN6
      .CSB(CSB),    //DIN5
      .MOSI(MOSI),   //DIN4
      .MISO(MISO),    //DOUT1
      .RST_Ctl_RB(RST_Ctl_RB),   //DIN7
      .Ctl_Clk_1(Ctl_Clk_1),    //DIN3
      .Ctl_Clk_2(Ctl_Clk_2),    //DIN2
      .Ctl_Load(Ctl_Load),     //DIN1 TODO fix conncection
      .Ctl_SIN(Ctl_SIN),     //DIN0
      .Ctl_SOut(Ctl_SOut),      //DOUT0

      .INJECTION(INJECTION),
      .DATA_LVDS(DATA_LVDS),     //LVDA_OUT0
      .Clk_ext_LVDS(Clk_ext_LVDS), //LVDA_IN0
      .Clk_ref(Clk_ref),      //LVDA_IN1
      .Sync_res(Sync_res),     //LVDA_IN2

      .RX_READY(RX_READY)
  );
// assign LED[1] = RX_READY;

endmodule
