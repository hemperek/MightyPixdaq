/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module receiver_logic
(
      input wire              RESET,
      input wire              WCLK, //this is the clock as the data comes in
      input wire              FCLK, //this is 10 times the data clock??
      input wire              BUS_CLK,
      input wire              RX_DATA,
      input wire              read,
      output wire  [31:0]     data,
      output wire             empty,
      output wire             full,
      output wire             rec_sync_ready,
      output reg  [7:0]       lost_err_cnt,
      output reg  [7:0]       decoder_err_cnt,
      output reg [15:0]       fifo_size,
      input wire              invert_rx_data,
      input wire              enable_rx,
      input wire              FIFO_CLK
);

wire RESET_WCLK;
cdc_reset_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RESET), .clk_out(WCLK), .pulse_out(RESET_WCLK));

reg enable_rx_buf, enable_rx_buf2, enable_rx_wclk;
always @ (posedge WCLK)
begin
    enable_rx_buf <= enable_rx;
    enable_rx_buf2 <= enable_rx_buf;
    enable_rx_wclk <= enable_rx_buf2;
end

// 8b/10b record sync
reg [9:0] raw10bdata;
always @(posedge FCLK) begin
    if (RESET_WCLK)
        raw10bdata <= 10'b0;
    else
        raw10bdata <= {raw10bdata[9:0], RX_DATA};
end

reg decoder_err;
wire [9:0] data_8b10b;
rec_sync rec_sync_inst (
    .reset(RESET_WCLK),
    .datain(RX_DATA),
    .data(data_8b10b),
    .WCLK(WCLK),
    .FCLK(FCLK),
    .rec_sync_ready(rec_sync_ready),
    .decoder_err(decoder_err)
);

wire write_8b10b;
assign write_8b10b = rec_sync_ready & enable_rx_wclk;

reg [9:0] data_to_dec;
integer i;
always @ (*) begin
    for (i=0; i<10; i=i+1)
        data_to_dec[(10-1)-i] = data_8b10b[i];
end

reg dispin;
wire dispout;
always@(posedge WCLK) begin
    if(RESET_WCLK)
        dispin <= 1'b0;
    else
        dispin <= dispout;

end

//this takes into account some of the special features from the chip
//that there are always 4 8 bit words containing data.
//one thing that might not work, since it is not standard 8b/10b encoded, is the alignment

wire dec_k;
wire [7:0] dec_data;
wire code_err, disp_err;
decode_8b10b decode_8b10b_inst (
    .datain(data_to_dec),
    .dispin(dispin),
    .dataout({dec_k,dec_data}), // control character, data out
    .dispout(dispout),
    .code_err(code_err),
    .disp_err(disp_err)
);

//check if data is aligned:

always@(posedge WCLK) begin
  if (!rec_sync_ready && dec_data!=8'hBC)
    decoder_err <= 1'b1;
  else
    decoder_err <= 1'b0;
end


//filter out idle patterns
reg [7:0] data_cleaner_input[0:3];
integer i_clean;
reg write_dec_in;
reg is_debug;
always@(posedge WCLK) begin
//always @ (dec_data) begin
    if(RESET_WCLK)
        write_dec_in <= 0;
    else
      if (dec_data!=8'hBC)
          begin
              data_cleaner_input[i_clean]=dec_data;
              //this is chip specific
              if ( dec_k && dec_data == 8'h3C )
                begin
                  is_debug<=1;
                  write_dec_in <= 0;
                end
              if (i_clean < 3)
                begin
                  i_clean<=i_clean+1;
                  write_dec_in <= 0;
                end
              else
                  begin
                      write_dec_in <= 1;
                      i_clean<=0;
                      is_debug<=0;
                  end
          end
end


wire cdc_fifo_full, cdc_fifo_empty;
always@(posedge WCLK) begin
    if(RESET_WCLK)
        lost_err_cnt <= 0;
    else
        if(cdc_fifo_full && (|write_dec_in) && lost_err_cnt != 8'hff)
            lost_err_cnt <= lost_err_cnt + 1;
        else
            lost_err_cnt <= lost_err_cnt;
end

//here one would have to add one additional 32bit word for a fpga timestamp
reg [1:0] data_header;
always @ ( posedge WCLK ) begin
  case ( data_cleaner_input[0] )
      8'h3C   : data_header = 2'b11; // this is K28.1 send counter 1
      8'h1C   : data_header = 2'b11; // not used
      8'hC0   : data_header = 2'b10; // Load pixel 2 and there are hits
      8'hC1   : data_header = 2'b01; // Read column 2
      default : data_header = 2'b00; // this should be //Read column 4 the 6 other bits are used
  endcase
end

//this would be a data word:
reg [31:0] data_cleaner_input_32bit;
always @ ( posedge write_dec_in ) begin
  data_cleaner_input_32bit = {data_header,data_cleaner_input[0][5:0],data_cleaner_input[1],data_cleaner_input[2],data_cleaner_input[3]};
end
// there are 5 options:
    //Send counter 1
    //Debug Output
    //DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0};

    //Load column 2
    //DataOut <= {8'h1C, 8'hAA, 8'h1C, 8'hAA};

    //Load pixel 2 and there are hits
    //DataOut<={8'hC0,timestamp_bin_counter[15:8],timestamp_gray_value[7:0]};

    //Read column 2
    //DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};

    //Read column 4
    //DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]};
//could also suppress Load column 2

wire [31:0] cdc_data_out;

// generate delayed and long reset
reg [5:0] rst_cnt;
always@(posedge BUS_CLK) begin
    if(RESET)
        rst_cnt <= 5'd8;
    else if(rst_cnt != 5'd7)
        rst_cnt <= rst_cnt +1;
end
wire rst_long = rst_cnt[5];
reg cdc_sync_ff;
always @(posedge WCLK) begin
    cdc_sync_ff <= rst_long;
end


cdc_syncfifo #(
    .DSIZE(32),
    .ASIZE(3)
) cdc_syncfifo_i (
    .rdata(cdc_data_out),
    .wfull(cdc_fifo_full),
    .rempty(cdc_fifo_empty),
    .wdata(data_cleaner_input_32bit),
    .winc(|write_dec_in & !cdc_fifo_full),
    .wclk(WCLK),
    .wrst(cdc_sync_ff),
    .rinc(!full),
    .rclk(FIFO_CLK),
    .rrst(rst_long)
);


reg fill_fifo;
always @ ( posedge WCLK ) begin
    fill_fifo = (!i_clean) && !(data_cleaner_input_32bit & 32'h1CAA1CAA);
end

wire [31:0] fifo_size_int;

gerneric_fifo #(
    .DATA_SIZE(32),
    .DEPTH(1024*8)
) fifo_i (
    .clk(FIFO_CLK),
    .reset(rst_long),

    .write(fill_fifo),
    .read(read),
    .data_in(data_cleaner_input_32bit),
    .full(full),
    .empty(empty),
    .data_out(data),
    .size(fifo_size_int)
);

always @(posedge FIFO_CLK) begin
    fifo_size <= {5'b0, fifo_size_int};
end

`ifdef SYNTHESIS_NOT
wire [35:0] control_bus;
chipscope_icon ichipscope_icon
(
    .CONTROL0(control_bus)
);

chipscope_ila ichipscope_ila
(
    .CONTROL(control_bus),
    .CLK(WCLK),
    .TRIG0({code_err, disp_err, dec_k,dec_data, data_8b10b})
);
`endif

endmodule
