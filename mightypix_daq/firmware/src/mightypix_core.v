
`timescale 1ns / 1ps
`default_nettype none

module mightypix_core (

    //local bus
    input wire BUS_CLK,
    inout wire [7:0] BUS_DATA,
    input wire [15:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    input wire BUS_RST,

    //clocks
    input wire CLK8,
    input wire CLK40,
    input wire CLK16,
    input wire CLK160,
    input wire CLK320,
    // input wire CLK400,

    //LED
    output wire [4:0] LED,

    output wire SCK,    //DIN6
    output wire CSB,    //DIN5
    output wire MOSI,   //DIN4
    input wire  MISO,    //DOUT1
    output wire RST_Ctl_RB,   //DIN7
    output wire Ctl_Clk_1,    //DIN3
    output wire Ctl_Clk_2,    //DIN2
    output wire Ctl_Load,     //DIN1 TODO fix conncection
    output wire Ctl_SIN,     //DIN0
    input wire  Ctl_SOut,      //DOUT0

    output wire INJECTION,
    input wire  DATA_LVDS,     //LVDA_OUT0
    output wire Clk_ext_LVDS, //LVDA_IN0
    output wire Clk_ref,      //LVDA_IN1
    output wire Sync_res,     //LVDA_IN2

    output wire RX_READY
    // input  wire INJECTION_IN,
);


assign SCK = 0;
assign CSB = 1;
assign MOSI = 0;
assign RST_Ctl_RB = 0; //for read back at some point

// -------  MODULE ADREESSES  ------- //
// DO NOT assign 32'h2000-32'h3000 it is for GPAC in mio3!!

localparam GPIO_BASEADDR = 16'h0010;
localparam GPIO_HIGHADDR = 16'h0100-1;

localparam PULSE_INJ_BASEADDR = 16'h4000;
localparam PULSE_INJ_HIGHADDR = 16'h5000-1;

localparam PULSE_GATE_TDC_BASEADDR = 16'h0400;
localparam PULSE_GATE_TDC_HIGHADDR = 16'h0500-1;

//how to do the data RX????
localparam DATA_RX_BASEADDR = 16'h0700;
localparam DATA_RX_HIGHADDR = 16'h0800-1;

// localparam TLU_BASEADDR = 16'h0600;
// localparam TLU_HIGHADDR = 16'h0700-1;

localparam TS_BASEADDR = 16'h0500;
localparam TS_HIGHADDR = 16'h0600-1;

// localparam TS_TLU_BASEADDR = 16'h0700;
// localparam TS_TLU_HIGHADDR = 16'h0800-1;

localparam TS_INJ_BASEADDR = 16'h0600;
localparam TS_INJ_HIGHADDR = 16'h0700-1;

// localparam TS_MON_BASEADDR = 16'h0700;
// localparam TS_MON_HIGHADDR = 16'h0800-1;

localparam SPI_BASEADDR = 16'h5000;
localparam SPI_HIGHADDR = 16'h8000-1;

localparam FIFO_BASEADDR = 16'h8000;
localparam FIFO_HIGHADDR = 16'h9000-2;

localparam  TIMESTAMP = 16'h0000;

localparam ABUSWIDTH = 16;
// -------  USER MODULES  ------- //
localparam VERSION = 8'h01;

reg RD_VERSION;
always@(posedge BUS_CLK)
    if(BUS_ADD == 16'h0000 && BUS_RD)
        RD_VERSION <= 1;
    else
        RD_VERSION <= 0;
assign BUS_DATA = (RD_VERSION) ? VERSION : 8'bz;

wire [15:0] GPIO_OUT;
gpio
#(
    .BASEADDR(GPIO_BASEADDR),
    .HIGHADDR(GPIO_HIGHADDR),
    .IO_WIDTH(16),
    .IO_DIRECTION(16'h7fff)
) gpio
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(GPIO_OUT)
    );

// leave the GPIO like monopix for now!! this needs to be cleaned up
wire RESET_CONF, LDDAC_CONF, LDPIX_CONF, SREN_CONF, EN_BX_CLK_CONF, EN_OUT_CLK_CONF, RESET_GRAY_CONF;
wire EN_TEST_PATTERN_CONF, EN_DRIVER_CONF, EN_DATA_CMOS_CONF, EN_GRAY_RESET_WITH_TIMESTAMP;

assign RESET_CONF = GPIO_OUT[0];
assign LDDAC_CONF = GPIO_OUT[1];
assign LDPIX_CONF = GPIO_OUT[2];
assign SREN_CONF = GPIO_OUT[3];

assign EN_BX_CLK_CONF = GPIO_OUT[4];
assign EN_OUT_CLK_CONF = GPIO_OUT[5];
assign RESET_GRAY_CONF = GPIO_OUT[6];
assign EN_TEST_PATTERN_CONF = GPIO_OUT[7];

assign EN_DRIVER_CONF = GPIO_OUT[8];
assign EN_DATA_CMOS_CONF = GPIO_OUT[9];
assign EN_GRAY_RESET_WITH_TIMESTAMP = GPIO_OUT[10];

wire CONF_CLK;
assign CONF_CLK = CLK8;

wire SCLK, SDI, SDO, SEN, SLD;
assign SDO = Ctl_SOut;
spi
#(
    .BASEADDR(SPI_BASEADDR),
    .HIGHADDR(SPI_HIGHADDR),
    .MEM_BYTES(1024)
    )  spi_conf
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(CONF_CLK),

    .SCLK(SCLK),
    .SDI(SDI),
    .SDO(SDO),
    .SEN(SEN),
    .SLD(SLD)
);

assign Ctl_Clk_1  = SCLK;
assign Ctl_Clk_2  = SCLK;
assign Ctl_SIN = SDI;
assign Ctl_Load = SLD;

wire GATE_TDC;

pulse_gen
#(
    .BASEADDR(PULSE_INJ_BASEADDR),
    .HIGHADDR(PULSE_INJ_HIGHADDR)
)     pulse_gen_inj(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK40),
    .EXT_START(GATE_TDC),
    .PULSE(INJECTION)
);


wire FE_FIFO_READ, FE_FIFO_EMPTY;
wire [31:0] FE_FIFO_DATA;

wire SPI_FIFO_READ;
wire SPI_FIFO_EMPTY;
wire [31:0] SPI_FIFO_DATA;
assign SPI_FIFO_EMPTY = 1;




//puts clock to output with enable
//complains otherwise
ODDR clk_bx_gate(.D1(EN_BX_CLK_CONF), .D2(1'b0), .C(CLK40), .CE(1'b1), .R(1'b0), .S(1'b0), .Q(Clk_ref) );

// reg nRST_reg;
// assign RST_Ctl_RB = nRST_reg;
// always@(negedge CLK40)
//     nRST_reg <= !RESET_CONF;

// assign EN_TEST_PATTERN = EN_TEST_PATTERN_CONF;
// assign EN_DRIVER = EN_DRIVER_CONF;
// assign RX_READY = EN_DATA_CMOS_CONF;

//TODO: readout
// assign RESET = 0;

// LED assignments
assign LED[0] = 0;
assign LED[1] = 0;
assign LED[2] = 1;
assign LED[3] = 0;
assign LED[4] = 0;


mightypix_data_rx #(
   .BASEADDR(DATA_RX_BASEADDR),
   .HIGHADDR(DATA_RX_HIGHADDR),
   .ABUSWIDTH      (ABUSWIDTH)
) mightypix_data_rx (


    .RX_CLKX2(CLK40),
    .RX_DATA(DATA_LVDS),
    .RX_CLKW(~CLK40),
    // .RX_READ(READ),
    // .RX_FREEZE(FREEZE),
    // .TIMESTAMP(TIMESTAMP),
    .RX_READY(RX_READY),
    .RX_8B10B_DECODER_ERR(),
    .RX_FIFO_OVERFLOW_ERR(),

    .FIFO_CLK(CLK40),
    .FIFO_READ(FE_FIFO_READ),
    .FIFO_EMPTY(FE_FIFO_EMPTY),
    .FIFO_DATA(FE_FIFO_DATA),

    .RX_FIFO_FULL(),
    .RX_ENABLED(),

    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR)
);


//todo add timestamp need arbiter


// assign LEMO_TX[0] = TLU_CLOCK; // trigger clock; also connected to RJ45 output
// assign LEMO_TX[1] = TLU_BUSY;  // TLU_BUSY signal; also connected to RJ45 output. Asserted when TLU FSM has
// assign LEMO_TX[2] = INJECTION_MON; // TX2 for mio, J502 for mio3

endmodule
