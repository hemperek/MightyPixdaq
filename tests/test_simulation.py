#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import yaml
import time
from bitarray import bitarray

from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean
from mightypix_daq.mightypix import MightyPix
import basil.dut

class TestSimulation(unittest.TestCase):

    def setUp(self):

        extra_defines = []  # Simulate only one double column

        root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        cocotb_compile_and_run(
            sim_files=[root_dir + '/tests/hdl/tb.sv'],
            extra_defines=extra_defines,
            sim_bus='basil.utils.sim.SiLibUsbBusDriver',
            include_dirs=(root_dir, root_dir + "/mightypix_daq/firmware/src", root_dir + "/tests/hdl"),
            # extra='EXTRA_ARGS += -g2012'
        )

        with open(root_dir + '/mightypix_daq/mightypix.yaml', 'r') as f:
            cnfg = yaml.safe_load(f)

        cnfg['transfer_layer'][0]['type'] = 'SiSim'
        # cnfg['hw_drivers'][0]['init']['no_calibration'] = True

        # dut = basil.dut.Dut(conf=cnfg)
        self.dut = MightyPix(conf=cnfg)

        self.dut.init()

    def test_configuration(self):

        pass

    def tearDown(self):
        self.dut.close()
        time.sleep(5)
        cocotb_compile_clean()


if __name__ == '__main__':
    unittest.main()
