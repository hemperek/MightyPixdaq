/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ps / 1ps


`include "mightypix_daq/firmware/src/mightypix_mio.v"

module tb (
    input wire FCLK_IN,

    //full speed
    inout wire [7:0] BUS_DATA,
    input wire [15:0] ADD,
    input wire RD_B,
    input wire WR_B,

    //high speed
    inout wire [7:0] FD,
    input wire FREAD,
    input wire FSTROBE,
    input wire FMODE
);

wire [19:0] SRAM_A;
wire [15:0] SRAM_IO;
wire SRAM_BHE_B;
wire SRAM_BLE_B;
wire SRAM_CE1_B;
wire SRAM_OE_B;
wire SRAM_WE_B;

wire [2:0] LEMO_RX;
assign LEMO_RX = 0;

wire DEF_CONF, SO_CONF, CLK_CONF, SI_CONF, LD_CONF, RST_N, RESET_BCID;
wire CLK_BX, CLK_OUT, INJECTION;

wire READ_A, READ_B, FREEZE_A, FREEZE_B, TOK_A, TOK_B, OUT_A, OUT_B;

mightypix_mio fpga (
    .FCLK_IN(FCLK_IN),

    .BUS_DATA(BUS_DATA),
    .ADD(ADD),
    .RD_B(RD_B),
    .WR_B(WR_B),
    .FDATA(FD),
    .FREAD(FREAD),
    .FSTROBE(FSTROBE),
    .FMODE(FMODE),

    .SRAM_A(SRAM_A),
    .SRAM_IO(SRAM_IO),
    .SRAM_BHE_B(SRAM_BHE_B),
    .SRAM_BLE_B(SRAM_BLE_B),
    .SRAM_CE1_B(SRAM_CE1_B),
    .SRAM_OE_B(SRAM_OE_B),
    .SRAM_WE_B(SRAM_WE_B)

/*
    .LEMO_RX(LEMO_RX),

    .SO_CONF(SO_CONF),
    .CLK_CONF(CLK_CONF),
    .DEF_CONF(DEF_CONF),
    .SI_CONF(SI_CONF),
    .LD_CONF(LD_CONF),
    .RST_N(RST_N),

    .INJECTION(INJECTION),

    .CLK_BX(CLK_BX),
    .CLK_OUT(CLK_OUT),
    .READ_A(READ_A),
    .READ_B(READ_B),
    .FREEZE_A(FREEZE_A),
    .FREEZE_B(FREEZE_B),
    .RESET_BCID(RESET_BCID),

    .TOK_A(TOK_A),
    .TOK_B(TOK_B),
    .OUT_A(OUT_A),
	.OUT_B(OUT_B),
    .HITOR_A(),
    .HITOR_B()
*/
);

//SRAM Model
reg [15:0] sram [1048576-1:0];
assign SRAM_IO = !SRAM_OE_B ? sram[SRAM_A] : 16'hzzzz;
always@(negedge SRAM_WE_B)
    sram[SRAM_A] <= SRAM_IO;

wire INJ_PULSE;
assign INJ_PULSE = INJECTION;




initial begin
    $dumpfile("/tmp/mightypix.vcd");
    $dumpvars(0);
end

endmodule
