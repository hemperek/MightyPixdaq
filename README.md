DAQ for MightyPix prototype based on [Basil](https://github.com/SiLab-Bonn/basil) framework.

## Required packages

- Install [conda](http://conda.pydata.org) for python and needed packages:
```bash
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
export PATH=$HOME/miniconda/bin:$PATH
conda update --yes conda
conda install --yes numpy bitarray pytest pyyaml numba mock matplotlib scipy pytables progressbar2
#load miniconda3 with:
source $HOME/miniconda/bin/activate

```

- Install [pySiLibUSB](https://github.com/SiLab-Bonn/pySiLibUSB) for USB support.
    * Be sure to allow read and write mode for USB with the udev rules
    * Reload with ```udevadm control --reload-rules && udevadm trigger```

- Download and install [Basil](https://github.com/SiLab-Bonn/basil) for data acquisition and generic firmware modules (tested with v2.4.4):
```bash
git clone https://github.com/SiLab-Bonn/basil
cd basil
python setup.py develop
cd ..
```


- Download and setup [mightypix_daq](https://gitlab.cern.ch/mightypixtestbeam/MightyPixdaq)
```bash
git clone https://gitlab.cern.ch/mightypixtestbeam/MightyPixdaq.git
cd MightyPixdaq
python setup.py develop
```

If you want to use Simulation install 
```
pip install cocotb cocotb_bus
conda install -c conda-forge iverilog
```


The code is based on the many developments [SiLab](https://github.com/SiLab-Bonn). Many thanks for all the work!
