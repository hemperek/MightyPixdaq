The main connections in the repository will be named as in the chip "documentation"
But here is a short list how they are named from the GPAC side for the MIO/MIO3


| chip side | fpga/GPAC side |
| ------ | ------ |
|SCK | DIN6                |
|CSB | DIN5                |
|MOSI | DIN4               |
|MISO | DOUT1              |
|RST_Ctl_RB | DIN7         |
|Ctl_Clk_1 | DIN3          |
|Ctl_Clk_2 | DIN2          |
|Ctl_Load | DIN1           |
|Ctl_SIN | DIN0            |
|Ctl_SOut | DOUT0         |
|INJECTION | INJECTION     |
|DATA_LVDS | LVDA_OUT0     |
|Clk_ext_LVDS | LVDA_IN0   |
|Clk_ref | LVDA_IN1        |
|Sync_res | LVDA_IN2       |


For the voltages look in the yaml file. But you basically have to remember the names as they are on the chip side.

More details can also be found here:
https://gitlab.cern.ch/padeken/mightypixrun2020_pcb
